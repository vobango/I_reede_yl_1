﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_reede_yl_1
{
    class Program
    {
        static List<Inimene> Inimesed = new List<Inimene>();
        static DateTime algus = new DateTime(1950, 1, 1);
        static DateTime lõpp = new DateTime(2000, 1, 1);
        static int vahemik = (lõpp - algus).Days;

        static void Main(string[] args)
        {
            Algseis();

            List<int> päevad = new List<int>();

            foreach (var isik in Inimesed)
            {
                DateTime sünnipäevMöödas = isik.Sünniaeg.AddYears(((DateTime.Now.Year) - isik.Sünniaeg.Year));
                DateTime sünnipäevTulekul = isik.Sünniaeg.AddYears(((DateTime.Now.Year + 1) - isik.Sünniaeg.Year));

                int tulemasVõiMöödas = (sünnipäevMöödas - DateTime.Now).Days < 0 ? 
                    (sünnipäevTulekul - DateTime.Now).Days : 
                    (sünnipäevMöödas - DateTime.Now).Days;
                päevad.Add(tulemasVõiMöödas);
            }

            int järgmine = päevad.IndexOf(päevad.Min());

            Console.WriteLine($"Järgmine sünnipäev on isikul {Inimesed[järgmine].Nimi} ({päevad.Min()} päeva pärast).");
            
        }

        static void Algseis()
        {
            string[] nimed =
             {
                "Rasmus",
                "Robin",
                "Artjom",
                "Martin",
                "Markus",
                "Nikita",
                "Romet",
                "Oliver",
                "Oskar",
                "Sander",
                "Sofia",
                "Maria",
                "Laura",
                "Sandra",
                "Lisandra",
                "Milana",
                "Anna",
                "Viktoria",
                "Emma",
                "Mirtel"
            };

            Random r = new Random();
            for (int i = 0; i < nimed.Length; i++)
            {
                Inimesed.Add(new Inimene(nimed[i], algus.AddDays(r.Next(vahemik))));
            }
            Inimesed.Add(new Inimene ("Kahtlane", DateTime.Parse("29.02.2000")));
        }

        public class Inimene
        {
            public string Nimi;
            public DateTime Sünniaeg;

            public Inimene(string nimi, DateTime sünniaeg) //Uus intisiaator
            {
                this.Nimi = nimi;
                this.Sünniaeg = sünniaeg;
            }

            public override string ToString()
            {
                return $"{this.Nimi} on sündinud {this.Sünniaeg}";
            }
        }
    }
}

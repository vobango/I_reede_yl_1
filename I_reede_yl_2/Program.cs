﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_reede_yl_2
{
    class Program
    {
        static int[,] Arvutabel = new int[10, 10];

        static void Main(string[] args)
        {
            AlgAndmed();

            Console.Write("Sisesta arv: ");
            int sisend = int.Parse(Console.ReadLine());

            bool vaste = false;

            int katsetus = 0;
            int vahe = 0;
            int lõppVastus = 0;
            int koord1 = 0;
            int koord2 = 0;

            //Otsime sisendit tabelist
            for (int i = 0; i < Arvutabel.GetLength(0); i++)
            {                 
                for (int j = 0; j < Arvutabel.GetLength(1); j++)
                {
                    if (Arvutabel[i,j] == sisend)
                    {
                        Console.WriteLine($"Arv {sisend} asub arvutabeli ruudus indeksiga [{i}, {j}].");
                        vaste = true;
                        break;
                    }
                }
                if (vaste)
                {
                    break;
                }
                
            }

            //Kui tabelis sisendit ei leidu
            if (!vaste)
            {
                Console.WriteLine($"Arvu {sisend} tabelist ei leitud. Otsime lähimat vastet...");
                for (int i = 0; i < Arvutabel.GetLength(0); i++)
                {
                    //Console.WriteLine("Rida {0}", i);
                    for (int j = 0; j < Arvutabel.GetLength(1); j++)
                    {
                        //Console.WriteLine("Lahter {0}", j);
                        katsetus = Math.Abs(sisend - Arvutabel[i, j]); //väikseim absoluutväärtuste vahe tähendab lähimat arvu.

                        if (vahe == 0)
                        {
                            //Console.WriteLine("Määrame uue väikseima vahe");
                            vahe = katsetus;
                        }
                        else
                        {
                            if (katsetus < vahe)
                            {
                                //Console.WriteLine("Leidsin uue väikseima vahe - {0}!", Arvutabel[i,j]);
                                vahe = katsetus;
                                lõppVastus = Arvutabel[i, j];
                                koord1 = i;
                                koord2 = j;
                            }
                            else if (katsetus == vahe) //juhuks, kui väikseim vahe on esimeses lahtris
                            {                                
                                lõppVastus = Arvutabel[i, j];
                                koord1 = i;
                                koord2 = j;
                            }
                        }
                    
                    }                    
                }

                Console.WriteLine($"Arvule {sisend} lähim arv tabelis on {lõppVastus} asukohaga [{koord1}, {koord2}].");
            }

        }

        static void AlgAndmed()
        {
            Random r = new Random(1); //Ära unusta sulge lõpuks tühjendada!!!
            for (int i = 0; i < Arvutabel.GetLength(0); i++)
            {
                for (int j = 0; j < Arvutabel.GetLength(1); j++)
                {
                    Arvutabel[i, j] = r.Next(0, 100); //Genereeritavate arvude vahemik määra siin
                }
            }            
        }
    }
}
